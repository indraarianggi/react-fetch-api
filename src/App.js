import React, { Component } from "react";
import "./App.css";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pictures: []
        };
    }

    componentWillMount() {
        fetch("https://randomuser.me/api/?results=30")
            .then(results => {
                return results.json();
            })
            .then(data => {
                let pictures = data.results.map(pic => {
                    return (
                        <div key={pic.email}>
                            <img
                                src={pic.picture.medium}
                                alt={pic.name.first}
                            />
                        </div>
                    );
                });
                this.setState({ pictures: pictures });
            });
    }

    render() {
        return <div className="App">{this.state.pictures}</div>;
    }
}

export default App;
